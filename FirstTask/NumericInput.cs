﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTask
{
	public class NumericInput : TextInput
	{
		private List<char> listOfNums = new List<char>
		{
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0'
		};

		public override void Add(char c)
		{
			if(listOfNums.Contains(c))
			{
				sb.Append(c);
			}
		}
	}
}
