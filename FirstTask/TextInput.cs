﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTask
{
	public class TextInput
	{
		protected StringBuilder sb = new StringBuilder();

		public virtual void Add(char c)
		{
			sb.Append(c);
		}

		public string GetValue()
		{
			return sb.ToString();
		}
	}
}
