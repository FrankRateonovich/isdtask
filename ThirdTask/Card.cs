﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdTask
{
	public class Card
	{
		public Values Value { get; set; }
		public Suits Suit { get; set; }

		public override string ToString()
		{
			return $"{Value.ToString()} Of {Suit.ToString()}";
		}
	}
}
