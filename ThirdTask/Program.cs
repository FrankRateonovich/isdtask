﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdTask
{
	class Program
	{
		static void Main(string[] args)
		{
			Deck deck = new Deck();

			Console.WriteLine("All cards\n");
			deck.PrintCards();

			Console.WriteLine("\nAll diamonds\n");
			deck.PrintDiamonds();

			Console.WriteLine("\nCards with no Clubs\n");
			deck.RemoveClubs();
			deck.PrintCards();

			Console.ReadKey();
		}
	}
}
