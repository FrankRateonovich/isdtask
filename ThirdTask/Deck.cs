﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThirdTask
{
	public class Deck
	{
		private List<Card> deck = new List<Card>();

		public Deck()
		{
			for (int i = 0; i < Enum.GetValues(typeof(Values)).Length; i++)
			{
				for (int j = 0; j < Enum.GetValues(typeof(Suits)).Length; j++)
				{
					deck.Add(new Card {
						Suit = (Suits)j,
						Value = (Values)i
					});
				}
			}
		}

		public void PrintCards()
		{
			foreach(var card in deck)
			{
				Console.WriteLine(card);
			}
		}

		public void RemoveClubs()
		{
			deck.RemoveAll(x => x.Suit == Suits.Clubs);
		}

		public void PrintDiamonds()
		{
			deck.Where(x => x.Suit == Suits.Diamonds).ToList().ForEach(card => Console.WriteLine(card));
		}
	}
}
