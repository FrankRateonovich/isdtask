﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask
{
	public class AlertService
	{
		private readonly IAlertDAO alertDao;

		public AlertService(IAlertDAO alertDao)
		{
			this.alertDao = alertDao;
		}

		public Guid RaiseAlert()
		{
			return alertDao.AddAlert(DateTime.Now);
		}

		public DateTime GetAlertTime(Guid id)
		{
			return alertDao.GetAlert(id);
		}
	}
}

