﻿using System;

namespace SecondTask
{
	public interface IAlertDAO
	{
		Guid AddAlert(DateTime time);

		DateTime GetAlert(Guid id);
	}
}
